/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author Łukasz
 */
public class NowyProdukt {
    NowyProdukt(Dane dane){
        JFrame frame = new JFrame("Dodaj - Produkt");
        Container kontener = frame.getContentPane();
        GridLayout layout = new GridLayout(5, 1);
        kontener.setLayout(new BoxLayout(kontener, BoxLayout.Y_AXIS));
        
        JTextField nazwa = new JTextField("Nazwa produktu", 20);
        kontener.add(nazwa);
        
        PrzedmiotLista lista = new PrzedmiotLista();
        for(int i=1; i<=dane.czesci.dlugosc(); i++){
            kontener.add(new NowyProduktPanel(lista, dane.czesci.numer(i)));
        }
        
        JButton zapisz = new JButton("OK");
        ActionListener zapisz_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                Produkt p = new Produkt(nazwa.getText(), lista);
                dane.produkty.dodaj(p);
                new ProduktOkno(p);
                frame.dispose();
            }
        };
        zapisz.addActionListener(zapisz_al);
        kontener.add(zapisz);
        
        frame.pack();
        frame.setVisible(true);
    }
   
}