/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.Container;
import javax.swing.BoxLayout;
import javax.swing.JFrame;

/**
 *
 * @author Łukasz
 */
public class WykorzystaneOkno {
    public WykorzystaneOkno(CzesciLista dane){
        JFrame frame = new JFrame("Wykorzystane");
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  ////zamkniecie programu na wyjsciu
        Container kontener = frame.getContentPane();
        //GridLayout layout = new GridLayout(3, 1);
        kontener.setLayout(new BoxLayout(kontener, BoxLayout.Y_AXIS));
        
        for(int i=1; i<=dane.dlugosc(); i++){
            kontener.add(new WykorzystanePanel(dane.numer(i)));
        }
           
        frame.pack();
        frame.setVisible(true);
    }
    
}