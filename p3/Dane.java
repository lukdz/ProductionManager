/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.io.Serializable;

/**
 *
 * @author Łukasz
 */
public class Dane implements Serializable{
    CzesciLista czesci;
    ProduktyLista produkty;
    Dane(CzesciLista czesci, ProduktyLista produkty)
    {
        this.czesci = czesci;
        this.produkty = produkty;
    }
    public CzesciLista czesci()
    {
        return czesci;
    }
    public ProduktyLista produkty()
    {
        return produkty;
    }
    
}
