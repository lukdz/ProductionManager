/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Łukasz
 */
class NowyProduktPanel extends JPanel {

    public NowyProduktPanel(PrzedmiotLista lista, Czesc numer) {
        GridLayout layout = new GridLayout(1, 2);
        this.setLayout(layout);
        
        JLabel nazwa_etykieta = new JLabel(numer.nazwa());
        this.add(nazwa_etykieta);
        
        JButton dodaj = new JButton("Dodaj");
        ActionListener zapisz_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                lista.dodaj(numer);
            }
        };
        dodaj.addActionListener(zapisz_al);
        this.add(dodaj);
    }
    
}
