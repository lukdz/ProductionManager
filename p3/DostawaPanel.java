/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Łukasz
 */
public class DostawaPanel extends JPanel{
    public DostawaPanel(Czesc dane){
        GridLayout layout = new GridLayout(1, 4);
        this.setLayout(layout);
        
        JLabel nazwa_etykieta = new JLabel(dane.nazwa());
        this.add(nazwa_etykieta);
        
        JTextField ilosc = new JTextField("0", 20);
        this.add(ilosc);
        
        ActionListener zapisz_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                dane.edycjailosc(Integer.parseInt(dane.ilosc())+Integer.parseInt(ilosc.getText()));
            }
        };
        ilosc.addActionListener(zapisz_al);
    }   
}
