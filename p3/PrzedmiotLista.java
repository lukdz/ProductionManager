/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.io.Serializable;

/**
 *
 * @author Łukasz
 */
public class PrzedmiotLista implements Serializable{
    PrzedmiotLista nast;
    Przedmiot dane;
    int dlugosc;
    int ilosc;
    PrzedmiotLista(){}
    PrzedmiotLista(Przedmiot dane, PrzedmiotLista nast)
    {
        this.dane = dane;
        this.nast = nast;
        dlugosc = 0;
    }
    public Przedmiot numer(int n)
    {
        if(n==0){
            return dane;
        }
        else{
            return nast.numer(n-1);
        }
    }
    public void dodaj(Przedmiot dane){
        this.nast = new PrzedmiotLista(dane, this.nast);
        dlugosc++;
    }
    public int dlugosc(){
        return dlugosc;
    }
    public int ilosc(int n){
        if(n==0){
            return ilosc;
        }
        else{
            return nast.ilosc(n-1);
        }
    }
    public void ilosc(int ilosc, int poz){
        if(poz==0){
            this.ilosc = ilosc;
        }
        else{
            nast.ilosc(ilosc, poz-1);
        }
    }
    
}
