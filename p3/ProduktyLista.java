/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.io.Serializable;

/**
 *
 * @author Łukasz
 */
class ProduktyLista implements Serializable{
    ProduktyLista nast;
    Produkt dane;
    int dlugosc;
    ProduktyLista(){}
    ProduktyLista(Produkt dane, ProduktyLista nast)
    {
        this.dane = dane;
        this.nast = nast;
        dlugosc = 0;
    }
    public Produkt numer(int n)
    {
        if(n==0){
            return dane;
        }
        else{
            return nast.numer(n-1);
        }
    }
    public void dodaj(Produkt dane){
        this.nast = new ProduktyLista(dane, this.nast);
        dlugosc++;
    }
    public void usun(Produkt dane){
        if(this.nast!=null){
        if(this.nast.dane.nazwa().equals(dane.nazwa())){
            this.nast=this.nast.nast;
        }
        else{
            nast.usun(dane);
        }
        dlugosc--;
        }
    }
    public int dlugosc(){
        return dlugosc;
    } 
}