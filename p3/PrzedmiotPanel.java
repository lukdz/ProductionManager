/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Łukasz
 */
public class PrzedmiotPanel extends JPanel{
    PrzedmiotPanel(PrzedmiotLista lista, int poz){
        this.setLayout(new GridLayout(1, 2));
        
        JLabel nazwa_etykieta = new JLabel(lista.numer(poz).nazwa());
        this.add(nazwa_etykieta);
        
        JTextField ilosc_etykieta = new JTextField(Integer.toString(lista.ilosc(poz)));
        ActionListener zapisz_al = new ActionListener(){
            public void actionPerformed(ActionEvent e){
                lista.ilosc(Integer.parseInt(ilosc_etykieta.getText()), poz);
            }
        };
        ilosc_etykieta.addActionListener(zapisz_al);
        this.add(ilosc_etykieta);
        
        
    }
    
}
