/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;


import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;


/**
 *
 * @author Łukasz
 */
public class PomocOkno {
    public PomocOkno(){
    JFrame frame = new JFrame("Pomoc");
    GridLayout layout = new GridLayout(7, 1);
    frame.setLayout(layout);
    
    JLabel n1_etykieta = new JLabel("Opcje Menadżera:");
    frame.add(n1_etykieta);
    JLabel n2_etykieta = new JLabel("-sprawdzanie stanu magazynu");
    frame.add(n2_etykieta);
    JLabel n3_etykieta = new JLabel("-wprowadzanie dostaw/wykorzystania");
    frame.add(n3_etykieta);
    JLabel n4_etykieta = new JLabel("-wprowdzanie nowych produktów i części");
    frame.add(n4_etykieta);
    JLabel n5_etykieta = new JLabel("-sprawdzanie, które części są na wyczerpaniu");
    frame.add(n5_etykieta);    
    JLabel n6_etykieta = new JLabel("-usuwanie produktów");
    frame.add(n6_etykieta);    
    JLabel n7_etykieta = new JLabel("-zapisywanie i odczytywanie z dysku bazy danych");
    frame.add(n7_etykieta);  
    

    frame.pack();
    frame.setVisible(true);
  }
    
}