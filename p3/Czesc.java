/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p3;

import java.io.Serializable;

/**
 *
 * @author Łukasz
 */
public class Czesc extends Przedmiot implements Serializable{
    String nazwa;
    int cena;
    public int ilosc;
    Czesc (String nazwa, int cena, int ilosc) { 
        this.nazwa=nazwa;
        this.cena=cena;
        this.ilosc=ilosc;
    }
    public void edycja (String nazwa, int cena, int ilosc) { 
        this.nazwa=nazwa;
        this.cena=cena;
        this.ilosc=ilosc;
    }
    public void edycjailosc ( int ilosc) { 
        this.ilosc=ilosc;
    }
    public String nazwa () { 
        return nazwa;
    }
    public String cena () { 
        return Integer.toString(cena);
    }
    public String ilosc () { 
        return Integer.toString(ilosc);
    }
    @Override public String toString(){
        return "nazwa:" + nazwa + " cena:" + cena + " ilosc:" + ilosc;
    }
}
